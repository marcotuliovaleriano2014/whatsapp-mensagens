window.onload = function() {
	document.getElementById("verspan").innerHTML = chrome.runtime.getManifest().version;
	console.log('eventos definidos');
	if(document.getElementById('desconectar')  != null){
		document.getElementById('desconectar').addEventListener('click', logout());
	}
	if(document.getElementById('conectar') != null){
		document.getElementById('conectar').addEventListener('click', login());
	}
	
};

function login() {
	console.log('login');
	sessionStorage.setItem('chaveAcesso', '1212');
	document.getElementById('loginTxt').disabled = true;
	document.getElementById('senhaTxt').disabled = true;
	document.getElementById('conectar').style.display = 'none';
	document.getElementById('desconectar').style.display = 'block';
	window.href = window.href;
}

function logout() {
	console.log('logout');
	sessionStorage.setItem('chaveAcesso', null);
	document.getElementById('loginTxt').disabled = false;
	document.getElementById('senhaTxt').disabled = false;
	document.getElementById('conectar').style.display = 'block';
	document.getElementById('desconectar').style.display = 'none';
	window.href = window.href;
	
}

/*document.getElementById("buttonPedido").addEventListener("click",function(){
	chrome.runtime.sendMessage({type: "button_openTabPedido",value:1});
});*/

document.getElementById("buttonWhatsapp").addEventListener("click",function(){
	chrome.runtime.sendMessage({type: "button_openTabWhatsapp",value:1});
});

document.getElementById("buttonTest").addEventListener("click",function(){
	chrome.runtime.sendMessage({type: "button_teste", value:1, menssagem: "ola"});
});


document.getElementById("criarContato").addEventListener("click",function(){
	const tel = document.getElementById("telefoneContato").value
	const nome = document.getElementById("nomeContato").value
	const data = {type: "criarContatoSheets", value:1, telefone:tel, nome:nome}
	console.log("criarContatoSheets",data)
	chrome.runtime.sendMessage(data);
});

document.getElementById("atualizarContato").addEventListener("click",function(){
	const tel = document.getElementById("telefoneContato").value
	const status = document.getElementById("statusContatoAtualizar").value
	const data = {type: "atualizarContatoSheets", value:1, telefone:tel, nome:status}
	console.log("atualizarContatoSheets",data)
	chrome.runtime.sendMessage(data);
});

document.getElementById("buscarQuantidadeContatoStatus").addEventListener("click",function(){
	const quantidade = document.getElementById("quantidadeContato").value
	const status = document.getElementById("statusContato").value
	const data = {type: "buscarQuantidadeContatoStatus", value:1, quantidade:quantidade, status:status}
	console.log("buscarQuantidadeContatoStatus",data)
	chrome.runtime.sendMessage(data);
});

document.getElementById("buscarQuantidadeContatoStatusEnviar").addEventListener("click",function(){
	const quantidade = document.getElementById("quantidadeContato").value
	const status = document.getElementById("statusContato").value
	const tempo = document.getElementById("tempoContato").value * 1000	
	const mensagem = document.getElementById("mensagemContato").value
	
	let sequencias
	
	if(document.getElementById("sequenciaContato").value === "(SITE)"){
		sequencias = ['Ola tudo bem? Esse é o whatsapp do estabelecimento |LOJA|'
		, 'Gostaria de conversar com o responsável']
	}else if(document.getElementById("sequenciaContato").value === "(IFOOD)"){
		sequencias = ['Olá, esse é o whatsapp do respensável pelo |LOJA|', 
		'Meu nome é Larissa, falo da Cliente Fiel.\n\nGostaria de uns minutinhos da sua atenção para falar sobre a nossa ferramenta de automatização de pedidos.\n\nTemos atendente virtual para o seu whatsapp, aplicativos personalizados, cardápio digital, entre outras funcionalidades para auxiliar e otimizar o seu estabelecimento.\n\nAcha que alguma dessas funcionalidades faz sentido para o seu negócio hoje?'
		, 'Queria te convidar a dar uma olhada no nosso site para ter uma noção de como podemos te ajudar a aumentar as suas vendas e que você voltasse aqui para me contar, o que acha?\n\nhttps://appclientefiel.com.br/?utm_source=ifood']
	}else if(document.getElementById("sequenciaContato").value === "(DMUCH)"){
		sequencias = ['Olá, esse é o whatsapp do respensável pelo |LOJA|', 
		'Meu nome é Larissa e estou falando da Cliente Fiel, somos uma plataforma de Delivery e já temos mais de 500 estabelecimentos cadastrados em nosso sistema. Cuidamos do delivery de empresas como , Acaí Concept e Jah do Açaí, entre outras marcas aí da sua região!\n\nTudo bem com você?'
		, 'Gostaria de uns minutinhos da sua atenção para falar sobre a nossa ferramente de automatização de pedidos.\nTemos atendente virtual para o seu whatsapp, aplicativos personalizados, cardápio digital, entre outras funcionalidades para auxiliar e otimizar o seu estabelecimento.\nAcha que alguma dessas funcionalidades faz sentido para o seu negócio hoje?']
	}else{		
		sequencias = document.getElementById("sequenciaContato").value.split(";")
	}
	const data = {type: "buscarQuantidadeContatoStatusEnviar", value:1, quantidade:quantidade, status:status, tempo:tempo, mensagem: mensagem, sequencia: sequencias.length ? true : false, sequenciasMensagens:sequencias, semData: document.getElementById("semData").checked}
	console.log("buscarQuantidadeContatoStatusEnviar",data)
	chrome.runtime.sendMessage(data);
});