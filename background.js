/*
This variable is responsible for keeping track of the active WhatsApp tab.
It is updated once the tab communicates with the background page (sends a type:start request)
*/

var dateNow = new Date;
var numeroJanela = -1;
var whatsapp_tab_id = -1;
var pedidos_aba_id = -1;
var chaveAutenticacao;
var urlDialogFlow = "https://us-central1-izza-lcmjrp.cloudfunctions.net/procIzzaSuporte"
var urlBase = "http://ws.appclientefiel.com.br/rest/";
var urlPedidos = "http://sistema.appclientefiel.com.br/web/controlepedidosfornecedor/ver";
var urlDominioSistema =  "http://sistema.appclientefiel.com.br";
//var urlBase = "http://52.86.148.125:8080/ClienteFiel/rest/";
//var audio = new Audio('https://clientefielsp.s3-sa-east-1.amazonaws.com/sons/ring.mp3');
var audio = new Audio(chrome.runtime.getURL("sounds/ring.mp3"));
let tempo_envio_mensagem_local = 10;


//quando inicia ele busca informações importantes
//1° qual numero do whatsapp logado
//2° quais os numeros de funcionarios da cliente fiel
var izza = false
var alertados_ForaDoHorario = [];

chrome.runtime.onMessage.addListener(function(recebi, sender, sendResponse) {
	console.log("\n\n\n------------------------------------inicio-----------------------------------------------")

	//RECEBE DO PLUGIN

	switch(recebi.type){
		case "button_teste":
			clientMessage({tipo : "text", numeroCelular: "553193539556@c.us", mensagem: recebi.menssagem});
			break;		
		case "button_openTabWhatsapp":
			abrirAbaWhatsapp();
			break;			
		case "criarContatoSheets":
			insert_value(recebi)
			break;
		case "atualizarContatoSheets":
			update_value(recebi)
			break;
		case "buscarQuantidadeContatoStatus":
			buscarQuantidadeContatoStatus(recebi)
			break;
		case "buscarQuantidadeContatoStatusEnviar":
			buscarQuantidadeContatoStatusEnviar(recebi)
			break;
	}
});




/* GOOGLE SHEETS INICIO */ 
var script_url = "https://script.google.com/macros/s/AKfycbwiaD5xzy7REc27pJL4nme1VesvamRKlFJHPuD9sQ/exec";
  
// Make an AJAX call to Google Script
function insert_value(data) {
  console.log("insert_value")

  var id1 = data.telefone
  var name = data.nome
  
  
  var url = script_url+"?callback=ctrlq&name="+name+"&id="+id1+"&action=insert";


  var request = jQuery.ajax({
	crossDomain: true,
	url: url ,
	method: "GET",
	dataType: "jsonp"
  });

}

function update_value(data){
  console.log("update_value")
  var id1  =	 data.telefone
  var name =   data.nome
  
  var url = script_url+"?callback=ctrlq&name="+name+"&id="+id1+"&action=update";


  var request = jQuery.ajax({
	crossDomain: true,
	url: url ,
	method: "GET",
	dataType: "jsonp"
  });

  
}

function delete_value(){
  $("#re").css("visibility","hidden");
   document.getElementById("loader").style.visibility = "visible";
  $('#mySpinner').addClass('spinner');
  var id1=	$("#id").val();
  var name= $("#name").val();
  
  
  var url = script_url+"?callback=ctrlq&name="+name+"&id="+id1+"&action=delete";


  var request = jQuery.ajax({
	crossDomain: true,
	url: url ,
	method: "GET",
	dataType: "jsonp"
  });

}
// print the returned data
function ctrlq(e) {

  
  $("#re").html(e.result);
  $("#re").css("visibility","visible");
  read_value();
  
}

async function buscarQuantidadeContatoStatus(data = {}) {	
  if(typeof data.quantidade === undefined){
	  var quantidade = 100
  }else{
	  var quantidade = parseInt(data.quantidade)
  }
  
  if(typeof data.status === undefined){
	  var status =   ''
  }else{
	  var status =   data.status
  }	
  
  console.log("data", data)
  let temp = 0;
  var arrayContatosEnviar = []
  let url = script_url+"?action=read"
  let semData = data.semData

  

  await $.getJSON(url, function (json) {
	  json = json.records

	  for(let i = 0; i < json.length; i++){
		if(json[i].STATUS == status && ( (json[i].ID+"").length >= 10 && (json[i].ID+"").length <= 11) && ( !semData || !json[i].TIME_STAMP.length == semData)){
			console.log("buscarQuantidadeContatoStatus entrei")
			if(temp == quantidade){
				break;
			}							
			arrayContatosEnviar.push(json[i])
			temp++;
		}
	  }

	  console.log("retornoGoogleSheets",json)	
	  console.log("retornoQuantidade", arrayContatosEnviar)	
	  localStorage.setItem("buscarQuantidadeContatoStatus", JSON.stringify(arrayContatosEnviar))
  });
}

async function buscarQuantidadeContatoStatusEnviar(data){
  
  //busca os x contatos com status
  await buscarQuantidadeContatoStatus(data)

  //caso nao tenha tempo definido, vai ser 60 segundos
  if(typeof data.mensagem === undefined){
	  var mensagem = "Ola, tudo bem?"
  }else{
	  var mensagem = data.mensagem
  }

  if(typeof data.tempo === undefined){
	  var tempo = 60000
  }else{
	  var tempo = parseInt(data.tempo)
  }

  const enviarContatos = JSON.parse(localStorage.getItem('buscarQuantidadeContatoStatus'))


  var quantidadeEnviar = enviarContatos.length
  var inicio = 0;
  const enviarIntervalo = setInterval(() => {
	  if(inicio < quantidadeEnviar){
			if(data.sequencia === true){

				if(!localStorage.getItem('comercialClienteFiel')){
					localStorage.setItem('comercialClienteFiel', '[]')
				}
				const contatosLocalStorage = JSON.parse(localStorage.getItem('comercialClienteFiel'))
				
				var tem;
				for(let i = 0; i < contatosLocalStorage.length; i++){
					tem = contatosLocalStorage[i].telefone === enviarContatos[inicio].ID
					if(tem){
						contatosLocalStorage[i].qtResposta = 0
						contatosLocalStorage[i].mensagens = data.sequenciasMensagens
						break
					}
				}
				if(!tem){
					const cliente = {"status": enviarContatos[inicio].STATUS ,"cidade": enviarContatos[inicio].CIDADE ,"telefone":enviarContatos[inicio].ID, "nome": enviarContatos[inicio].NAME, "qtResposta": 0, "mensagens": data.sequenciasMensagens}
					contatosLocalStorage.push(cliente)
				}

				/*replaces
					|LOJA|
					|CIDADE|
				*/
				let proximaMensagem = data.sequenciasMensagens[0]
				proximaMensagem = proximaMensagem.replace("|LOJA|", enviarContatos[inicio].NAME[0].toLocaleUpperCase() + enviarContatos[inicio].NAME.substring(1).toLocaleLowerCase())
				proximaMensagem = proximaMensagem.replace("|CIDADE|", enviarContatos[inicio].CIDADE[0].toLocaleUpperCase() + enviarContatos[inicio].CIDADE.substring(1).toLocaleLowerCase())
				
				
				localStorage.setItem('comercialClienteFiel', JSON.stringify(contatosLocalStorage))
				sendMessage(proximaMensagem, enviarContatos[inicio].ID)


				if(!enviarContatos[inicio].STATUS || enviarContatos[inicio].STATUS === "SEM CONTATO"){
					//SEM STATUS OU STATUS IGUAL A SEM CONTATO
					update_value({telefone: enviarContatos[inicio].ID, nome:"PRIMEIRO CONTATO"})
				}else if(enviarContatos[inicio].STATUS != "AGENDADO" || enviarContatos[inicio].STATUS != "DESINTERESSADO"){
					//TEM STATUS e nao é AGENDADO ou DESINTERESSADO
					update_value({telefone: enviarContatos[inicio].ID, nome:"FALLOWUP"})
				}

			}else{
				sendMessage(mensagem,enviarContatos[inicio].ID)
			}		
			inicio++			  
	  }else{
		  clearInterval(enviarIntervalo);
	  }		
  }, tempo);
}


function read_value() {

$("#re").css("visibility","hidden");
 
 document.getElementById("loader").style.visibility = "visible";
var url = script_url+"?action=read";

$.getJSON(url, function (json) {

  // Set the variables from the results array
 
 


	  // CREATE DYNAMIC TABLE.
	  var table = document.createElement("table");

	  

	  var header = table.createTHead();
	  var row = header.insertRow(0);     
	  var cell1 = row.insertCell(0);
	  var cell2 = row.insertCell(1);
  
	  cell1.innerHTML = "<b>ID</b>";
	  cell2.innerHTML = "<b>Name</b>";
	  
	  // ADD JSON DATA TO THE TABLE AS ROWS.
	  for (var i = 0; i < json.records.length; i++) {

		  tr = table.insertRow(-1);
			  var tabCell = tr.insertCell(-1);
			  tabCell.innerHTML = json.records[i].ID;
			  tabCell = tr.insertCell(-1);
			  tabCell.innerHTML = json.records[i].NAME;
		  }
	

	  // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
	  var divContainer = document.getElementById("showData");
	  divContainer.innerHTML = "";
	  divContainer.appendChild(table);
	  document.getElementById("loader").style.visibility = "hidden";
	  $("#re").css("visibility","visible");
  });
}

/* GOOGLE SHEETS FIM */ 










function processMessage(data) {
	console.log("processMessage")
	//console.log("data: " + JSON.stringify(data))	

	//verifica se whatsapp esta logado e se o numero do whatsapp logado é da izza				
	if (isLoggedActive()) {
		
		//verifica se chegou nulo a informações da mensagem
		if (data != null) {
			const obj = { 
			  "appNome"   :  "cliente_fiel", 
			  "numeroCelular"   :  data.chatId._serialized,
			  "telefone"   :  data.chatId.user.substring(2),
			  "mensagem"   :  data.content, 
			  "idMensagem"   :  data.id, 
			  "idRemetente"   :  data.to.user,
			  "privado": data.chat.id.server === "c.us",
			  "nome" : data.sender.formattedName
			};	
			
			if(obj.privado){
				console.log("privado", obj)
				if(!localStorage.getItem('comercialClienteFiel')){
					localStorage.setItem('comercialClienteFiel', "[]")
				   }
	
				const contatosLocalStorage = JSON.parse(localStorage.getItem('comercialClienteFiel'))
	
				var tem;
				for(let i = 0; i < contatosLocalStorage.length; i++){

					//var id2 = obj.telefone+"".substring(0,2) + obj.telefone+"".substring(3,obj.telefone+"".length) //sem 9
					//var id3 = obj.telefone+"".substring(0,2) + "9" + obj.telefone+"".substring(3,obj.telefone+"".length) //com 9
					 
					tem = ((contatosLocalStorage[i].telefone+"").includes((obj.telefone+"").substring(3))) //|| (contatosLocalStorage[i].telefone == id2) || (contatosLocalStorage[i].telefone == id3)
					if(tem){
						contatosLocalStorage[i].qtResposta++

						//MOSTROU INTERESSE
						let temp1 = contatosLocalStorage[i].telefone
						if(temp1.length < 11){
							temp1 = temp1.substring(0,2) + "9" + temp1.substring(2)
						}						
						if(contatosLocalStorage[i].qtResposta === 6 && !contatosLocalStorage[i].status != "AGENDADO" && !contatosLocalStorage[i].status != "DESINTERESSADO" && !contatosLocalStorage[i].status != "CLIENTE"){
							update_value({telefone: temp1, nome:"INTERESSADO"})
							sendMessageGrupo('Interessado wa.me/'+ obj.numeroCelular.replace('@c.us',''),'553195522991-1587678119@g.us')
							
						}
						if(contatosLocalStorage[i].qtResposta === 1 && !contatosLocalStorage[i].status != "AGENDADO" && !contatosLocalStorage[i].status != "DESINTERESSADO" && !contatosLocalStorage[i].status != "CLIENTE"){
							update_value({telefone: temp1, nome:"RESPONDEU"})							
						}
						

						if(contatosLocalStorage[i].mensagens.length > contatosLocalStorage[i].qtResposta){

							let proximaMensagem = contatosLocalStorage[i].mensagens[contatosLocalStorage[i].qtResposta]

							/*replaces
								|LOJA|
								|CIDADE|
							*/
	
							proximaMensagem = proximaMensagem.replace("|LOJA|", contatosLocalStorage[i].nome[0].toLocaleUpperCase() + contatosLocalStorage[i].nome.substring(1).toLocaleLowerCase())
							proximaMensagem = proximaMensagem.replace("|CIDADE|",contatosLocalStorage[i].cidade[0].toLocaleUpperCase() + contatosLocalStorage[i].cidade.substring(1).toLocaleLowerCase())
	
							sendMessage(proximaMensagem, obj.telefone)
						}	
						localStorage.setItem('comercialClienteFiel', JSON.stringify(contatosLocalStorage))

						break
					}
				}				
			}

		} else {
			console.log('processMessage data nulo: ' + data );
			console.log("------------------------------------fim-----------------------------------------------\n\n")
		}
	}
}



































chrome.notifications.onButtonClicked.addListener(acaoBotoes);

chrome.runtime.onStartup.addListener(function () {
	console.log("Plugin Startup");			

});

chrome.runtime.onInstalled.addListener(function () {
	console.log("Plugin instalado");
	 inicializarTabs();
	//setTimeout(inicializarTabs, 5000);
});


function inicializarTabs() {
	whatsapp_tab_id = -1;
	pedidos_aba_id = -1;
	numeroJanela = -1;
	var abas = new Array();

	chrome.windows.getCurrent(function(win) {
    	chrome.tabs.getAllInWindow(win.id, function(tabs) {
	        abas = tabs;
			console.debug(abas);

			var i;
			for (i = 0; i < abas.length; i++) {
				var aba = abas[i];
			    if (~aba.url.indexOf("web.whatsapp.com")) {
			    	whatsapp_tab_id = aba.id;
			    }

			    if (~aba.url.indexOf("sistema.appclientefiel.com.br")) {
			    	pedidos_aba_id = aba.id;
			    }
		  	}

		  	if (pedidos_aba_id == -1) {
				chrome.tabs.create({ url: urlDominioSistema, index : 0 });	
				pedidos_aba_id = 0;			
			} else {
				chrome.tabs.update(pedidos_aba_id,{url: urlDominioSistema});
			}

			if (whatsapp_tab_id == -1) {
				chrome.tabs.create({ url: 'https://web.whatsapp.com/', index : 1});
			} else {
				chrome.tabs.update(whatsapp_tab_id,{url:"https://web.whatsapp.com"});
			}

		});
	});
}
 

chrome.webNavigation.onCompleted.addListener(function(details) {

	/*Adiciona os scripts na página do whatsapp*/
	if (~details.url.indexOf("https://web.whatsapp.com/")) {
		console.log("Abriu a página web.whatsapp.com");	
		whatsapp_tab_id = details.tabId;
		chrome.tabs.executeScript(details.tabId, {file: "js/content.js"});
	}

	if (~details.url.indexOf("pedidosfornecedor")) {
		console.log("Abriu a página de recebimento dos pedidos");	
		pedidos_aba_id = details.tabId;
	}

	/*Obtém o cookie da página do cliente fiel*/
	if (~details.url.indexOf("52.86.148.125") || ~details.url.indexOf("sistema.appclientefiel.com.br")) {
		console.log("Abriu a página sistema.appclientefiel.com.br" );	
		if (whatsapp_tab_id != -1) {
			getCookieIZZA();
		} else {
			console.log('Página do whatsapp ainda não aberta.')
		}
	}

	
});


function removeCookieIZZA() {
	chrome.cookies.remove({"url": urlDominioSistema, "name": "CHAVEIZZAONLINE"}, function(cookie) {
        console.log("cookie removido: " + cookie)
    });
}

function getCookieIZZA() {
    chrome.cookies.get({"url": urlDominioSistema, "name": "CHAVEIZZAONLINE"}, function(cookie) {
			cookie = 'bWFyY290dWxpb3ZhbGVyaWFubzIwMTRAZ21haWwuY29tOjIwMTZsaW1vbmFkYQ=='
	        if (cookie) {
		        ID = cookie.value;
		        var chaveAtual = localStorage.getItem('chaveAcesso');
		        console.log("chave salva:" + chaveAtual);
		        console.log("cookie lido:" + ID);
		        /*Caso a chave do cookie seja diferente da atual, atualiza a chave e reinicia a página do whatsapp*/
		        if (chaveAtual == null || chaveAtual != ID) {
		        	localStorage.setItem('chaveAcesso', ID);	
		        	clientMessage({tipo : "atualizar"});
		        }
		      }
	 });
}


/*
Passes a message to the client (dict-obj)
Full journey from here - to content script, then to the webpage through the DOM
*/
function clientMessage(data) {
	console.log("clientMessage: ", JSON.stringify(data));
	chrome.tabs.sendMessage(whatsapp_tab_id, data);
}

/*
Listening to messages from the content script (webpage -> content script -> *backgrund page* -> host)
*/
chrome.runtime.onMessage.addListener(function(data, sender) {
	
	if (sender.tab) {
		
		switch(data.tipo) {		
		  case "init":
		    console.log('Init recebido da página do whatsapp');
		    init();
		    break;
		  case "logout":
		    console.log('Logout recebido da página do whatsapp');
		    logout();
		    break;
		  case "start":
		  	console.log('Start recebido da página do whatsapp');
		    start();
		    break;
		  case "pause":
		    console.log('Pause recebido da página do whatsapp');
		    pause();
		    break;
		  default:
		    processMessage(data);
		}
	}
});

function isLoggedActive() {
	return localStorage.getItem('chaveAcesso') != null && localStorage.getItem('pause') == null;
}

function isLogged() {
	return localStorage.getItem('chaveAcesso') != null;
}

/*function requestServerPost(obj, path) {
	var ajax = new XMLHttpRequest();
	var url = urlBase + path;

	ajax.open("POST", url, true);
	ajax.setRequestHeader("Access-Control-Allow-Origin", "*");
	ajax.setRequestHeader("Access-Control-Allow-Headers", "Content-type");
	ajax.setRequestHeader("Content-type", "application/json");
	ajax.setRequestHeader("Authorization", "Basic " + localStorage.getItem('chaveAcesso'));
	ajax.send(JSON.stringify(obj));

	// Cria um evento para receber o retorno.
	ajax.onreadystatechange = function() {
	  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
	  
		if (ajax.readyState == 4) {
			if (ajax.status == 200) {
				var data = JSON.parse(ajax.responseText);			    
				console.log('Mensagem enviada ao servidor: ' + obj.idMensagem);
			    clientMessage(data);
			} else if (ajax.status == 401) {
				console.log('Chamando logout.');
				logout();	
			} else {
				console.log("Status:" + ajax.status);
				clientMessage(data);
			}
		} 
	}	
}*/


function cheksIsNotNull(obj) {
  return obj !== null && obj !== undefined
}

function getNewLocalMessages(){
	if (isLoggedActive()) {
		var item = localStorage.getItem('lastMessage');
		var lastMessage = JSON.parse(item);
		if(lastMessage !== null && lastMessage !== undefined && lastMessage.lista !== null && lastMessage.lista !== undefined && lastMessage.lista.length !== 0){

			
			var novaLista = lastMessage.lista;
			
			var restantes = lastMessage.lista.map(function(obj){

				if(obj !== null){

					let currentDate = new Date();
					let lastMessageDate = new Date(obj.data_last_message);

					let difference = (currentDate - lastMessageDate) / 1000;
					
					if(difference >= tempo_envio_mensagem_local){
						var mensagens = obj.listObj.map(function(item){
							return  item.mensagem;
						});

						var texto = "";
						for(var i = 0 ; i < mensagens.length ; i++){
							texto += mensagens[i] + " ";
						}


						console.log("ENVIANDO MENSAGEM: " + texto);

						var objToRequest = obj.listObj[0];
						objToRequest.mensagem = texto;

						//requestServerPost(objToRequest, 'bot/RegistrarMensagem');
						return null;
					}else{
						console.log("-- VAI ENVIAR em: " + (tempo_envio_mensagem_local -  parseInt(difference)) + " SEGUNDOS -> ");
						return obj;
					}
				}
				
			});

			if(restantes !== null && restantes !== undefined && restantes.length !== 0){
				for(var i = 0 ; i < restantes.length ; i ++){
					var item = restantes[i];
					if(item === null || item === undefined){
						restantes.splice(i,1);
					}
				}
			}else{
				restantes = [];
			}
			
			console.log("Clientes na Fila de Envio:  " + restantes);
			lastMessage.lista = restantes;
			localStorage.setItem('lastMessage', JSON.stringify(lastMessage));
			
		}else{
			//console.log("NADA A ENVIAR");
		}
	}
	setTimeout(getNewLocalMessages,2000);
}

function createLista(obj, lastMessage){
	let numeroCelular = obj.numeroCelular;
	lastMessage = {lista : [{
				telefone: numeroCelular,
				listObj: [obj],
				data_last_message: new Date()
			}]};

	localStorage.setItem('lastMessage', JSON.stringify(lastMessage));
	console.log("MENSAGEM INSERIDA - INIT");

}

function addMessageToNewNumber(obj, listaMaster){
	let numeroCelular = obj.numeroCelular;
	var listLastMessage = listaMaster.lista;
	
	var lastMessageSingle = {
			telefone: numeroCelular,
			listObj: [obj],
			data_last_message: new Date()
		};
	
	listLastMessage.push(lastMessageSingle); //importante ser //push

	listaMaster.lista = listLastMessage;
	localStorage.setItem('lastMessage', JSON.stringify(listaMaster));
	console.log("MENSAGEM INSERIDA -> Número NOVO");

}
function addMessageToNumber(obj, lastMessage){
	let numeroCelular = obj.numeroCelular;
	var listaMaster = JSON.parse(lastMessage);
	var listLastMessage = listaMaster.lista;

	var numberExist = false;
	for(var i = 0 ; i < listLastMessage.length ; i ++){
		var last = listLastMessage[i];	
		if(last.telefone === numeroCelular){
			
			var lista = last.listObj;
			lista.push(obj); //importante ser //push

			last.data_last_message = new Date();
			last.listObj = lista

			listLastMessage[i] = last;
			numberExist = true;
			break;
		}
	}

	if(!numberExist){
		addMessageToNewNumber(obj, listaMaster);
		return;
	}

	listaMaster.lista = listLastMessage;
	localStorage.setItem('lastMessage', JSON.stringify(listaMaster));
	console.log("MENSAGEM INSERIDA -> Número Existente");
}

function adicionarMensagemNaFila(obj){
	let numeroCelular = obj.numeroCelular;

	var lastMessage = localStorage.getItem('lastMessage');
	//console.log("VAMOS VER: " + lastMessage);
	if(lastMessage === [] || lastMessage === null || lastMessage === undefined || lastMessage.isEmpty || lastMessage.length === 0 ){
		createLista(obj, lastMessage);
	}else{
		addMessageToNumber(obj, lastMessage)
	}
}

function sendMessageGrupo(mensagem, destinatario){
	//var json = {"tipo" : "text", "numeroCelular" : "5531973306335@c.us", "mensagem" : mensagem};
	var json = {"tipo" : "text", "numeroCelular" : destinatario, "mensagem" : mensagem};
	clientMessage(json);
}

function sendMessage(mensagem, destinatario){
	//var json = {"tipo" : "text", "numeroCelular" : "5531973306335@c.us", "mensagem" : mensagem};
	var json = {"tipo" : "text", "numeroCelular" : "55"+destinatario+"@c.us", "mensagem" : mensagem};
	clientMessage(json);
}

function init() {
	if (localStorage.getItem('chaveAcesso') != null && localStorage.getItem('pause') == null) {
		clientMessage({tipo : "logado"});
		
		var ajax = new XMLHttpRequest();
		var url = urlBase + "bot/ScriptInicial";
			
		ajax.open("GET", url, true);
		ajax.setRequestHeader("Access-Control-Allow-Origin", "*");
		ajax.setRequestHeader("Access-Control-Allow-Headers", "Content-type");
		ajax.setRequestHeader("Content-type", "application/json");
		ajax.setRequestHeader("Authorization", "Basic " + localStorage.getItem('chaveAcesso'));
		ajax.send();

		// Cria um evento para receber o retorno.
		ajax.onreadystatechange = function() {
		  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
			if (ajax.readyState == 4) {
				if (ajax.status == 200) {
					var lista = JSON.parse(ajax.responseText);
					var i;
					for (i = 0 ; i < lista.length;  i++) {
						console.log(lista[i]);
						clientMessage(lista[i]);			
					}			    
				} else if (ajax.status == 401) {
					console.log('chamando logout');
					logout();	
				} else {
					console.log("Status:" + ajax.status);
				}
			}
		}	
		setTimeout(sendWaitMessage,5000);


	} else if (localStorage.getItem('chaveAcesso') != null && localStorage.getItem('pause') != null) {
		clientMessage({tipo : "pausado"});	
	} else {
		clientMessage({tipo : "deslogado"});
		waitLogin();
	}
}

function sendWaitMessage() {
	clientMessage({tipo : "aguardarmensagem"});
}

function waitLogin() {
	console.log("Aguardando login");
	
	try {
		getCookieIZZA();
		if (localStorage.getItem('chaveAcesso') != null) {
			console.log("Logado, atualizando tela.");
			clientMessage({tipo : "atualizar"});	
			return true;
		}
	}  catch(e) {
		console.log(e);
	}

	setTimeout(waitLogin,3000);
}

function start() {
	localStorage.removeItem('pause');
	clientMessage({tipo : "logado"});
}

function pause() {
	localStorage.setItem('pause', true);
	clientMessage({tipo : "pausado"});
}

function logout() {
	localStorage.removeItem('chaveAcesso');
	localStorage.removeItem("lastMessage");
	removeCookieIZZA();
	clientMessage({tipo : "deslogado"});
	waitLogin();
}



function getNewMessages() {
	try {
		if (isLoggedActive()) {
			console.log("Buscando Mensagens");
			//console.log('Buscando novas mensagens: ' + localStorage.getItem('chaveAcesso'));

			var ajax = new XMLHttpRequest();
			var url = urlBase + "bot/BuscarMensagem";
				
			ajax.open("GET", url, true);
			ajax.setRequestHeader("Content-type", "application/json");
			ajax.setRequestHeader("Authorization", "Basic " + localStorage.getItem('chaveAcesso'));
			ajax.send();

			// Cria um evento para receber o retorno.
			ajax.onreadystatechange = function() {
			  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
				if (ajax.readyState == 4) {
					if (ajax.status == 200) {
						var lista = JSON.parse(ajax.responseText);
						var i;
						for (i = 0 ; i < lista.length;  i++) {
							console.log(lista[i]);
							clientMessage(lista[i]);			
						}			    
					} else if (ajax.status == 401) {
						console.log('chamando logout');
						logout();	
					} else {
						console.log("Status:" + ajax.status);
					}
				}
			}	
		}
	} catch(e) {
		console.log(e);
	}

	setTimeout(getNewMessages, 5000);
}

function abrirAbaWhatsapp(){
	whatsapp_tab_id = -1;
	var abas = new Array();

	chrome.windows.getCurrent(function(win) {
    	chrome.tabs.getAllInWindow(win.id, function(tabs) {
	        abas = tabs;
			console.debug(abas);
			var i;
			for (i = 0; i < abas.length; i++) {
				var aba = abas[i];
			    if (~aba.url.indexOf("web.whatsapp.com")) {
			    	whatsapp_tab_id = aba.id;
			    }
			  }
		  	if (whatsapp_tab_id == -1) {
				chrome.tabs.create({ url: 'https://web.whatsapp.com/', index : 1}, function(abaId) {whatsapp_tab_id = abaId;});		
			} else {
				//força o foco na tela de pedidos
				chrome.tabs.get(whatsapp_tab_id, function(tab) {
					chrome.tabs.highlight({'tabs': tab.index}, function() {});
				})
			}

		});
	});
}

function abrirAbaPedidos() {
	pedidos_aba_id = -1;
	var abas = new Array();

	chrome.windows.getCurrent(function(win) {
    	chrome.tabs.getAllInWindow(win.id, function(tabs) {
	        abas = tabs;
			console.debug(abas);

			var i;
			for (i = 0; i < abas.length; i++) {
				var aba = abas[i];
			    if (~aba.url.indexOf("pedidosfornecedor")) {
			    	pedidos_aba_id = aba.id;
			    }
			  }
			  

		  	if (pedidos_aba_id == -1) {
				chrome.tabs.create({index : 0, windowId : win.id,  url: urlPedidos }, function(abaId) {pedidos_aba_id = abaId;});	
			} else {

				//força o foco na tela de pedidos
				chrome.tabs.get(pedidos_aba_id, function(tab) {
					chrome.tabs.highlight({'tabs': tab.index}, function() {});
				})
			}

		});
	});
}

function getNewPedidos() {
	try {
		if (isLogged()) {
			//console.log('Buscando novos pedidos: ' + localStorage.getItem('chaveAcesso'));			
			var ajax = new XMLHttpRequest();
			var url = urlBase + "integracao/eventspolling";	
			ajax.open("GET", url, true);
			ajax.setRequestHeader("Access-Control-Allow-Origin", "*");		
			ajax.setRequestHeader("Content-type", "application/json");
			ajax.setRequestHeader("Authorization", "Basic " + localStorage.getItem('chaveAcesso'));
			ajax.send();

			// Cria um evento para receber o retorno.
			ajax.onreadystatechange = function() {
			  // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
				if (ajax.readyState == 4) {
					if (ajax.status == 200) {
						var lista = JSON.parse(ajax.responseText);
						var i;
						for (i = 0 ; i < lista.length;  i++) {
							var n = lista[i];
							
							if (n.code == 'PLACED' && n.statusPedido == 0) {
								abrirAbaPedidos();
								pausarSomNavegador();
								

								var options =  {
													type: 'basic',
													title: 'Você tem um novo pedido #' + n.correlationId,
													message: 'Aceite pela página de pedidos no navegador.',
													iconUrl: 'logo/32.png',
													buttons: [{title : 'ABRIR SISTEMA DE PEDIDOS'}]											
												};

								console.log(JSON.stringify(n));
								console.log("Solicitação: " + n.code + " Vias: " + n.QuantidadeVias + " ExigirConfirmacao: " + n.ExigirConfirmacao);
								//chrome.tabs.executeScript( pedidos_aba_id, {code:"document.getElementById('audiopedido').pause()"});
								
								  
								audio.play();
								console.log("Ativei som plugin")
								chrome.notifications.create('' + n.correlationId, options, function(data) {console.log('notify: ' + data);} );
							}
						}			    
					} else if (ajax.status == 401) {
						console.log('chamando logout');
						logout();	
					} else {
						console.log("Status:" + ajax.status);
					}
				}
			}	
		}
	} catch(e) {
		console.log(e);
	}

	setTimeout(getNewPedidos, 5000);
}



function permissaoNotificacao() {
	// Let's check if the browser supports notifications
	if (!("Notification" in window)) {
		console.log("Browser nao esta permitido para criar notificações")
	}
  
	// Let's check if the user is okay to get some notification
	else if (Notification.permission === "granted") {
	  // If it's okay let's create a notification
	  console.log("Permitido criar notificações")
	}
  
	// Otherwise, we need to ask the user for permission
	else if (Notification.permission !== 'denied') {
	  Notification.requestPermission(function (permission) {
		// If the user is okay, let's create a notification
		if (permission === "granted") {
			console.log("Se usuario der ok é permitido criar notificações")
		}
	  });
	}
  
	// At last, if the user already denied any notification, and you 
	// want to be respectful there is no need to bother them any more.
  }


function acaoBotoes(notificationId, buttonIndex) {
	console.log('SolicitacaoId: ' + notificationId);
	console.log('button: ' + buttonIndex);

	//envia script para a pagina.

	chrome.tabs.get(pedidos_aba_id, function(tab) {
		chrome.tabs.highlight({'tabs': tab.index}, function() {});
	})
	  
	//chrome.tabs.executeScript( pedidos_aba_id, {code:"aceitarImprimir(" + notificationId + ");"}, function(results){ console.log(results + "Aceitou pelo plugin"); });

	if (buttonIndex == 0) {
		console.log('clicou no primeiro botão');
	}

	if (buttonIndex == 1) {
		console.log('clicou no segundo botão');
	}
	
}

function pausarSomNavegador() {
	// pausa o som do navegador quando usado a izza
	pedidos_aba_id = -1;
	var abas = new Array();

	chrome.windows.getCurrent(function(win) {
    	chrome.tabs.getAllInWindow(win.id, function(tabs) {
	        abas = tabs;
			console.debug(abas);

			var i;
			for (i = 0; i < abas.length; i++) {
				var aba = abas[i];
			    if (~aba.url.indexOf("pedidosfornecedor")) {
			    	pedidos_aba_id = aba.id;
			    }
		  	}

		  	if (pedidos_aba_id >= -0) {
				chrome.tabs.executeScript(pedidos_aba_id,{
					code: "document.getElementById('audiopedido').src = '';document.getElementById('audiopedido').pause();"
				  });								  
				console.log("Pausei som navegador")
			} 

		});
	});
}

(function() {
	//getNewMessages();
	//getNewPedidos();	
	//getNewLocalMessages();
})();

//verifica a permisao de notificacao
//permissaoNotificacao()